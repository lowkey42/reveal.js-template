function createCeLink(language, code) {
    let compiler = "clang_lifetime";
    let options = "-std=c++17 -Wextra -Wall -pedantic -Wextra-semi -Wzero-as-null-pointer-constant -Wold-style-cast -Wno-unused-parameter -Wno-unused-private-field -Wno-missing-braces -Wno-error-unused-command-line-argument";
	let content = [];
	content.push({
		type: 'component',
		componentName: 'codeEditor',
		componentState: {
			id: 1,
			source: code,
			options: {compileOnChange: true, colouriseAsm: true},
			fontScale: 1.5
		}
	});
	content.push({
		type: 'component',
		componentName: 'executor',
		componentState: {
			source: 1,
			compilationPanelShown: false,
			options: options,
			compiler: compiler,
			fontScale: 1.5
		}
	});
	let obj = {
		version: 4,
		content: [{type: 'row', content: content}]
	};
	let ceFragment = encodeURIComponent(JSON.stringify(obj));

	return "https://godbolt.org/#" + ceFragment;
}

function runCE(elem) {
	var preElem = elem.parentElement;
	var linkElem = elem.nextElementSibling;
	var ceLink = linkElem.href.replace('godbolt.org/#', 'godbolt.org/e?hideEditorToolbars=true#');
	
	var iframe = document.createElement("iframe");
	iframe.width = preElem.offsetWidth;
	iframe.height = preElem.offsetHeight + 40;
	iframe.src = ceLink;
	preElem.parentElement.replaceChild(iframe, preElem);
}


import Markdown from '../reveal.js/plugin/markdown/markdown.esm.js';
import Notes from '../reveal.js/plugin/notes/notes.esm.js';
import Search from '../reveal.js/plugin/search/search.esm.js';
import Highlight from '../reveal.js/plugin/highlight/highlight.esm.js';

var marked = Markdown().marked;

const renderer = new marked.Renderer();
let orgCode = renderer.code.bind(renderer);
const CODE_LINE_NUMBER_REGEX = /\[([\s\d,|-]*)\]/;

renderer.code = ( code, language ) => {
    let lineNumbers = '';

    // Users can opt in to show line numbers and highlight
    // specific lines.
    // ```javascript []        show line numbers
    // ```javascript [1,4-8]   highlights lines 1 and 4-8
    if( CODE_LINE_NUMBER_REGEX.test( language ) ) {
        lineNumbers = language.match( CODE_LINE_NUMBER_REGEX )[1].trim();
        lineNumbers = `data-line-numbers="${lineNumbers}"`;
        language = language.replace( CODE_LINE_NUMBER_REGEX, '' ).trim();
    }

	var ceLink = createCeLink(language, code);

    return orgCode(code, language, false).replace('<code', `<code data-trim ${lineNumbers} onClick="runCE(this)" `).replace('</pre>', `<a href="${ceLink}" target="_blank" rel="noopener noreferrer" class="ce_link">CE</a></pre>`);
};

const urlParams = new URLSearchParams(window.location.search);

Reveal.initialize({
    slideNumber: true,
    controlsTutorial: false,
    controlsBackArrows: 'faded',
    width: 1280,
	height: 720,
	pdfSeparateFragments: false,
	showNotes: urlParams.has("print-pdf") || urlParams.has("pdf") ? 'separate-page' : false,
	slideNumber: 'c/t',
    markdown: {
    	renderer
    },
	plugins: [ Markdown, Highlight, Notes, Search ],
/*	dependencies: [
		{ src: './plugins/chalkboard/chalkboard.js' }
	],
	keyboard: {
		67: function() { RevealChalkboard.toggleNotesCanvas() },	// toggle notes canvas when 'c' is pressed
		66: function() { RevealChalkboard.toggleChalkboard() },	// toggle chalkboard when 'b' is pressed
		46: function() { RevealChalkboard.clear() },	// clear chalkboard when 'DEL' is pressed
		 8: function() { RevealChalkboard.reset() },	// reset chalkboard data on current slide when 'BACKSPACE' is pressed
		68: function() { RevealChalkboard.download() },	// downlad recorded chalkboard drawing when 'd' is pressed
		88: function() { RevealChalkboard.colorNext() },	// cycle colors forward when 'x' is pressed
		89: function() { RevealChalkboard.colorPrev() },	// cycle colors backward when 'y' is pressed
	}
	*/
});


